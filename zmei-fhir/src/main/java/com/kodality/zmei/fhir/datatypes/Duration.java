package com.kodality.zmei.fhir.datatypes;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Duration extends Quantity {

}
