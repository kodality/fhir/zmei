package com.kodality.zmei.fhir.client;

public class FhirClientException extends RuntimeException {

  public FhirClientException(Throwable e) {
    super(e);
  }

}
